module.exports = {
  head: {
    title: 'АИС КП НСО',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'front part of personal cabinet' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon-32x32.png', sizes: '32x32' }
    ]
  },
  loading: { color: '#3B8070' },
  css: ['~assets/scss/default.scss'],
  modules: [
    '@nuxtjs/style-resources',
  ],
  plugins: [
    { src: '~/plugins/axios' },
    { src: '~/plugins/element-ui' },
    { src: '~/plugins/local-storage', ssr: false },
  ],
  styleResources: {
    scss: [
      './assets/scss/vars.scss',
      './assets/scss/mixins.scss'
    ]
  },
  router: {
    middleware: 'check-before-move'
  },
  mode: 'spa',
  /*
  ** Удаление console.log из production
  */
  build: {
    terser: {
      terserOptions: {
        compress: {
          drop_console: true
        }
      }
    },
    /*
    ** Run ESLint on save
    */
    extend(config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
  },

  env: {
    baseUrl: process.env.NODE_ENV === 'development'
      ? 'http://ias-api.cloud.nstu.ru'
      : ''
  }

};

