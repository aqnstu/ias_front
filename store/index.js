import Vue from 'vue';
import Vuex from 'vuex';
import storeDictionary from './modules/dictionary.store'
import storeUser from './modules/user.store'
import storeEmployer from './modules/employer.store'
import storeInvest from './modules/invest.store'
import storeEducation from './modules/education.store'
import storeLot from './modules/lot.store'
import storeAdmin from './modules/admin.store'

Vue.use(Vuex);

export default function () {
  return new Vuex.Store({
    modules: {
      // Global modules:
      'user': storeUser,

      // Isolated modules: (use moduleName/methods
      'employer': storeEmployer,
      'admin': storeAdmin,
      'dictionary': storeDictionary,
      'invest': storeInvest,
      'education': storeEducation,
      'lot': storeLot,
      // ...
    },
    actions: {
      resetStore({ commit }) {
        commit('employer/reset');
        commit('dictionary/reset');
        commit('invest/reset');
        commit('lot/reset');
      },
    },
    strict: false,
  });
};
