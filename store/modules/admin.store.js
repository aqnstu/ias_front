import {
  API,
  convertResponseToInner,
  sendGetRequest,
  sendPostFileRequest,
  sendPostRequest
} from "../../interfaces/request";
import { ADMIN, INVESTING_TYPE } from "../../interfaces/entities";

const state = () => ({
  lists: {
    projects: [],
  },
});

export const getters = {};

export const mutations = {
  setList(state, data = { list, value }) {
    state.lists[data.list] = data.value;
  },
  reset(state) {
    state.lists = {
      projects: [],
    };
  },
};

export const actions = {

  async getList({ commit }, { entityType, tabName, page }) {
    switch (entityType) {
      case ADMIN.VO: {
        const response = await sendGetRequest(API.HOST + '/api/list/vo');
        return response ? response.data : [];
      }
      case ADMIN.SPO: {
        const response = await sendGetRequest(API.HOST + '/api/list/spo');
        return response ? response.data : [];
      }
      case ADMIN.REF_LIST: {
        const response = await sendGetRequest(API.HOST + `/api/get/${tabName}?page_num=${page}`);
        return convertResponseToInner(response.data);
      }
      case ADMIN.REF_ENTITY: {
        const response = await sendGetRequest(API.HOST + `/api/get/${tabName}`);
        return response ? response.data : [];
      }
    }
  },

  async saveEntity({ commit }, { form, entityType, id, row, tabName  }) {
    switch (entityType) {
      case ADMIN.VO: {
        return await sendPostRequest(API.HOST + '/api/set/organization/' + id, form);
      }
      case ADMIN.SPO: {
        return await sendPostRequest(API.HOST + '/api/set/organization/' + id, form);
      }
      case ADMIN.REF_ENTITY: {
        return await sendPostRequest(API.HOST + `/api/set/${tabName}/` + id, row);
      }
    }
  },

};

export default {
  namespaced: true, // this module is isolated
  state,
  mutations,
  actions,
  getters,
};

export const strict = false;
