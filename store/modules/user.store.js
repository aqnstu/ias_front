import {API, sendAuthRequest, sendGetRequest, sendPostRequest} from "../../interfaces/request";
import axios from "axios";

const state = () => ({
  userData: {
    name: '',
    role: '',
    id_organization: 0,
    organization: '',
    is_spo: 0,
  },
  token: '',
  tempKey: '', //для получения токена
});

export const getters = {
  isLoggedIn: (state, getters) => {
    return state.token !== '';
  },
  hasTempKey: (state, getters) => {
    return state.tempKey !== '';
  },
  tempKey: (state, getters) => {
    return state.tempKey;
  },
  iasToken: (state, getters) => {
    return state.token;
  },
  idOrganization: (state, getters) => {
    return state.userData.id_organization;
  },
  organization: (state, getters) => {
    return state.userData.organization;
  },
  isSpo: (state, getters) => {
    return state.userData.is_spo;
  },
};

export const mutations = {
  setUserData(state, data = { field, value }) {
    state.userData[data.field] = data.value;
  },
  setToken(state, token) {
    state.token = token;
    axios.defaults.headers.common['AUTH-TOKEN'] = token;
  },
  setTempKey(state, tempKey) {
    state.tempKey = tempKey;
  },
};

export const actions = {
  async login({ commit }, { login, password }) {
    const result = await sendAuthRequest(login, password);
    console.log('AUTH', result);
    if (!result.data || !result.data.login) {
      throw 'login';
    }
    commit('setUserData', { field: 'name', value: result.data.greeting });
    commit('setUserData', { field: 'role', value: result.data.role });
    commit('setUserData', { field: 'id_organization', value: result.data.id_organization });
    commit('setUserData', { field: 'organization', value: result.data.organization });
    commit('setUserData', { field: 'is_spo', value: result.data.is_spo });
    commit('setToken', result.data.token);
  },
  async login_by({ commit }, token) {
    const result = await sendPostRequest(API.HOST + '/api/auth_key', { token: token });
    console.log('AUTH', result);
    if (!result.data || !result.data.login) {
      throw 'login_by';
    }
    commit('setUserData', { field: 'name', value: result.data.greeting });
    commit('setUserData', { field: 'role', value: result.data.role });
    commit('setUserData', { field: 'id_organization', value: result.data.id_organization });
    commit('setUserData', { field: 'organization', value: result.data.organization });
    commit('setUserData', { field: 'is_spo', value: result.data.is_spo });
    commit('setToken', result.data.token);
  },
  async logout({ commit, dispatch }) {
    // const result = await sendAuthRequest(login, password);
    commit('setUserData', { field: 'role', value: '' });
    commit('setUserData', { field: 'name', value: '' });
    commit('setUserData', { field: 'id_organization', value: 0 });
    commit('setUserData', { field: 'organization', value: '' });
    commit('setUserData', { field: 'id_spo', value: 0 });
    commit('setToken', '');
    commit('setTempKey', '');
    dispatch('resetStore');
  },
  async newTempKey({ commit, dispatch }, val){
    commit('setTempKey', val);
  },
  async esiaURL({ commit, dispatch }, tempKey){
    let result = await sendGetRequest(API.HOST + `/api/auth/esia_url/${tempKey}`, {});
    return result ? result.data : None;
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
};

export const strict = false;
