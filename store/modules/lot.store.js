import {
  API,
  convertResponseToInner,
  sendGetRequest,
  sendPostFileRequest,
  sendPostRequest
} from "../../interfaces/request";
import { EDU_TYPE, LOT } from "../../interfaces/entities";
import axios from "axios";

const state = () => ({
  isDataOutdated: false,
  scenarioId: null,
  yearId: 2019,
  adapterScenarioId: null,
  lists: {
    scenario: [],
    paramsTab1: [],
    paramsTab2: [],
    paramsTab3: [],
    result1: [],
    result2: [],
    result3: [],
    result4: [],
    result5: [],
    result6: [],
    result7: [],
    resultYears: [],
  },
});

export const getters = {};

export const mutations = {
  setStatusOutdated(state, status) {
    state.isDataOutdated = !!status;
  },
  setScenario(state, scenarioId) {
    state.scenarioId = scenarioId;
  },
  setYear(state, yearId) {
    state.yearId = yearId;
  },
  setAdapterScenario(state, adapterScenarioId) {
    state.adapterScenarioId = adapterScenarioId;
  },
  setList(state, data = { list, value }) {
    state.lists[data.list] = data.value;
  },
  reset(state) {
    state.lists = {
      scenario: [],
      paramsTab1: [],
      paramsTab2: [],
      paramsTab3: [],
      result1: [],
      result2: [],
      result3: [],
      result4: [],
      result5: [],
      result6: [],
      result7: [],
      resultYears: [],
    };
  },
};

export const actions = {
  async getList({ commit }, { entityType, tabId, scenarioId, year, page, filter_str, tabName, pageSize, organization, mrigo, okved, year_prognoz, okved_parts }) {
    let result = null;
    let listName = '';
    switch (entityType) {
      case LOT.SCENARIO_LIST : {
        listName = 'scenario';
        result = await sendGetRequest(API.HOST + '/api/refs/fc_scenario', {});
      }
        break;
      case LOT.SCENARIO_TAB : {
        listName = `paramsTab${tabId}`;
        result = await sendGetRequest(API.HOST + `/api/get/fc_tab${tabId}/${scenarioId}`, {});
      }
        break;
      case LOT.RESULT_LINE : {
        listName = `result${tabId}`;
        result = await sendGetRequest(API.HOST + `/api/get/lines/fc_res${tabId}/${scenarioId}`, {});
      }
        break;
      case LOT.RESULT_YEARS : {
        listName = `resultYears`;
        result = await sendGetRequest(API.HOST + `/api/get/lines/fc_scenario_years/${scenarioId}`, {});
      }
        break;
      case LOT.KCP_OKSO_RESULT : {
        result = await sendGetRequest(API.HOST + `/api/list/kcp_okso_result?year=${year}`, {});
        return result ? result.data : [];
      }
      case LOT.KCP_LOT_RESULT : {
        result = await sendGetRequest(API.HOST + `/api/list/kcp_lot_result?year=${year}`, {});
        return result ? result.data : [];
      }
      case LOT.KCP_LOT_RESULT2 : {
        result = await sendGetRequest(API.HOST + `/api/list/kcp_lot_result2?year=${year}`, {});
        return result ? result.data : [];
      }
      case LOT.KCP_LOT_LIST : {
        result = await sendGetRequest(API.HOST + `/api/list/kcp_lot?year=${year}`, {});
        return result ? result.data : [];
      }
      case LOT.KCP_LOT_REQUEST : {
        result = await sendGetRequest(API.HOST + `/api/list/kcp_request?year=${year}`, {});
        return result ? result.data : [];
      }

      case LOT.KCP_LOT_REQUEST2 : {
        result = await sendGetRequest(API.HOST + `/api/list/kcp_request_lot?year=${year}`, {});
        return result ? result.data : [];
      }

      case LOT.KCP_LOT_REQUEST_ONE : {
        result = await sendGetRequest(API.HOST + `/api/get/kcp_request/${tabId}`, {});
        return result ? result.data : [];
      }
      case LOT.KCP_LOT_REQUEST_PARAMS : {
        if (tabId === undefined) return [];
        result = await sendGetRequest(API.HOST + `/api/get/lines/kcp_request_lines/${tabId}`, {});
        return result ? result.data : [];
      }
      case LOT.KCP_LOT_REQUEST_SRC : {
        if (tabId === undefined) return [];
        result = await sendGetRequest(API.HOST + `/api/list/kcp_request_src/${tabId}/${page}`, {});
        return result ? result.data : [];
      }
      case LOT.KCP_LOT_REQUEST_PARAMS_OKSO : {
        if (tabId === undefined) return [];
        result = await sendGetRequest(API.HOST + `/api/get/lines/kcp_request_okso/${tabId}`, {});
        return result ? result.data : [];
      }
      case LOT.ADAPTER_CLASS_LIST : {
        if (filter_str == undefined || filter_str == '')
          result = await sendGetRequest(API.HOST + `/api/list/adapter_class?page_num=${page}&is_spo=1`, {});
        else
          result = await sendGetRequest(API.HOST + `/api/list/adapter_class?page_num=${page}&is_spo=1&filter_str=${filter_str}`, {});
        return convertResponseToInner(result.data);
      }
      case LOT.POLL_VOSPO_PIVOT : {
        result = await sendGetRequest(API.HOST + `/api/pivot/poll_students`, {});
        return result ? result.data : [];
      }
      case LOT.POLL_SCHOOL_PIVOT : {
        result = await sendGetRequest(API.HOST + `/api/pivot/poll_school`, {});
        return result ? result.data : [];
      }
      case LOT.PIVOT_VACS : {
        result = await sendGetRequest(API.HOST + `/api/pivot/vacs`, {});
        return result ? result.data : [];
      }
      case LOT.PIVOT_RESUME : {
        result = await sendGetRequest(API.HOST + `/api/pivot/resume`, {});
        return result ? result.data : [];
      }
      case LOT.PIVOT_PROGNOZ_KCP : {
        result = await sendGetRequest(API.HOST + `/api/pivot/prognoz_kcp/${scenarioId}`, {});
        return result ? result.data : [];
      }
      case LOT.PIVOT_PROGNOZ_UGSN : {
        result = null;
        if (year)
          result = await sendGetRequest(API.HOST + `/api/pivot/prognoz_ugsn/${scenarioId}?year=${year}`, {});
        else
          result = await sendGetRequest(API.HOST + `/api/pivot/prognoz_ugsn/${scenarioId}`, {});
        return result ? result.data : [];
      }

      case LOT.PROCESS_UGSN : {
        result = null;
        result = await sendGetRequest(API.HOST + `/api/process_ugsn`, {});
        return result ? result.data : [];
      }

      case LOT.PIVOT_PROGNOZ_WP : {
        result = null;
        if (year)
          result = await sendGetRequest(API.HOST + `/api/pivot/prognoz_wp/${scenarioId}?year=${year}`, {});
        else
          result = await sendGetRequest(API.HOST + `/api/pivot/prognoz_wp/${scenarioId}`, {});
        return result ? result.data : [];
      }
      case LOT.PIVOT_PROGNOZ_OKPDTR : {
        year = year?year:0;
        organization = organization?organization:0;
        mrigo = mrigo?mrigo:0;
        okved = okved?okved:0;
        year_prognoz = year_prognoz?year_prognoz:0;
        result = await sendGetRequest(API.HOST + `/api/pivot/prognoz_okpdtr/${scenarioId}?year_prognoz=${year_prognoz}&year=${year}&organization=${organization}&mrigo=${mrigo}&okved=${okved}`, {});
        return result ? result.data : [];
      }
      case LOT.UGS_DEDUCT : {
        result = await sendGetRequest(API.HOST + `/api/get/ugsn_deduct`, {});
        return result ? result.data : [];
      }

      case LOT.EXPERT_VALS : {
        result = await sendGetRequest(API.HOST + `/api/get/experts_vals`, {});
        return result ? result.data : [];
      }
      case LOT.EXPERT_VALS_OKVED : {
        result = await sendGetRequest(API.HOST + `/api/get/experts_vals_okved`, {});
        return result ? result.data : [];
      }
      case LOT.TYPE_SER_OKVED : {
        result = await sendGetRequest(API.HOST + `/api/get/type_ser_line_okved`, {});
        return result ? result.data.map(v => ({...v, isModified: false})) : [];
      }
      case LOT.FORECAST_BTR : {
        result = await sendGetRequest(API.HOST + `/api/list/btr_forecast?id_ser_scenario=${scenarioId}`, {});
        return result ? result.data : [];
      }
      case LOT.LOADED_BTR : {
        result = await sendGetRequest(API.HOST + `/api/list/btr_loaded/${year}`, {});
        return result ? result.data : [];
      }
      case LOT.MONITORING_FORM12 : {
        result = await sendGetRequest(API.HOST + `/api/list/monitoring_form12?year=${year}&page=${page}&page_size=${pageSize}&filter_str=${filter_str}`, {});
        return result ? result.data : [];
      }
      case LOT.MONITORING_FORM12_PVT : {
        result = await sendGetRequest(API.HOST + `/api/list/monitoring_form12_pivot?year=${year}&okved_parts=${okved_parts}`, {});
        return result ? result.data : [];
      }
      case LOT.MONITORING_FORM13 : {
        result = await sendGetRequest(API.HOST + `/api/list/monitoring_form13?year=${year}&page=${page}&page_size=${pageSize}&filter_str=${filter_str}`, {});
        return result ? result.data : [];
      }
      case LOT.MONITORING_FORM14 : {
        result = await sendGetRequest(API.HOST + `/api/list/monitoring_form14?year=${year}&page=${page}&page_size=${pageSize}&filter_str=${filter_str}`, {});
        return result ? result.data : [];
      }
      case LOT.MONITORING_FORM15 : {
        result = await sendGetRequest(API.HOST + `/api/list/monitoring_form15?year=${year}&page=${page}&page_size=${pageSize}&filter_str=${filter_str}`, {});
        return result ? result.data : [];
      }
      case LOT.MONITORING_FORM16 : {
        result = await sendGetRequest(API.HOST + `/api/list/monitoring_form16?year=${year}&page=${page}&page_size=${pageSize}&filter_str=${filter_str}`, {});
        return result ? result.data : [];
      }
      case LOT.MONITORING_FORM17 : {
        result = await sendGetRequest(API.HOST + `/api/list/monitoring_form17?year=${year}&page=${page}&page_size=${pageSize}&filter_str=${filter_str}`, {});
        return result ? result.data : [];
      }
      case LOT.SER_LINES : {
        result = await sendGetRequest(API.HOST + `/api/list/ser_lines`, {});
        return result ? result.data : [];
      }
      case LOT.STAT_DATASETS : {
        result = await sendGetRequest(API.HOST + `/api/list/loaded`, {});
        return result ? result.data : [];
      }
      case LOT.PIVOT_STAT_DATASET : {
        result = await sendGetRequest(API.HOST + `/api/pivot/loaded/${tabName}`, {});
        return result ? result.data : [];
      }
    }
    commit('setList', {
      list: listName,
      value: result ? result.data : []
    });
  },
  async saveFile({ commit }, {entityType, tabName, year, id, file}){
    switch (entityType) {
      case LOT.PIVOT_STAT_DATASET:
        return await sendPostFileRequest(API.HOST + `/api/upload/stat/${tabName}/${year}`, file);
      case LOT.KCP_LOT_REQUEST:
        return await sendPostFileRequest(API.HOST + `/api/upload/kcp_request/${year}`, file);
    }

    return null;
  },
  async saveEntity({ commit }, { form, entityType, tabId, scenarioId, id }) {
    switch (entityType) {
      case LOT.KCP_LOT_REQUEST:
        return await sendPostRequest(API.HOST + `/api/set/kcp_request/${id}`,
          LOT.convertRequestToOutput(form));
      case LOT.KCP_LOT_REQUEST_PARAMS:
        return await sendPostRequest(API.HOST + `/api/set/lines/kcp_request_lines/${id}`,
          LOT.convertRequestLinesToOutput(form));
      case  LOT.KCP_LOT_REQUEST_PARAMS_OKSO:
        return await sendPostRequest(API.HOST + `/api/set/lines/kcp_request_okso/${id}`,
          LOT.convertRequestOKSOToOutput(form));
      case LOT.SCENARIO_TAB:
        return await sendPostRequest(API.HOST + `/api/set/fc_tab${tabId}/${scenarioId}`, form);
      case LOT.KCP_LOT_LIST:
        return await sendPostRequest(API.HOST + `/api/set/kcp_lot/${id}`,
          LOT.convertLotToOutput(form));
      case LOT.KCP_LOT_REQUEST_SRC:
        return await sendPostRequest(API.HOST + `/api/set_kcp_request_src/${id}`, [...form]);
      case LOT.ADAPTER_CLASS:
        return await sendPostRequest(API.HOST + `/api/set/adapter_class/${id}`, { ...form });
      case LOT.UGS_DEDUCT:
        return await sendPostRequest(API.HOST + `/api/set/ugsn_deduct/${id}`, { ...form });
      case LOT.EXPERT_VALS:
        return await sendPostRequest(API.HOST + `/api/set/experts_vals/${id}`, { ...form });
      case LOT.EXPERT_VALS_OKVED:
        return await sendPostRequest(API.HOST + `/api/set/experts_vals_okved/${id}`, { ...form });
      case LOT.TYPE_SER_OKVED:
        return await sendPostRequest(API.HOST + `/api/set/type_ser_line_okved/${id}`, { ...form });
    }
    return null;
  },
  async makeAction({ commit }, {entityType, year}) {
    switch (entityType) {
      case LOT.KCP_LOT_PROCESS:
        return await sendPostRequest(API.HOST + `/api/process/process_kcp_req_spo`, `year=${year}`);
    }
    return null;
  },
  async getDownload({ commit }, { tabId, scenarioId, format }) {
    if (format === LOT.FORMAT_WORD) {
      return await sendGetRequest(API.HOST + `/api/report2/fc_res${tabId}/${scenarioId}?type=docx`);
    }
    if (format === LOT.FORMAT_EXCEL) {
      return await sendGetRequest(API.HOST + `/api/report2/fc_res${tabId}/${scenarioId}?type=xlsx`);
    }

    return null;
  },
  async getDownloadKcpRequest({ commit }, { tabId }) {
      return await sendGetRequest(API.HOST + `/api/gen_request/${tabId}`);
  },
};

export default {
  namespaced: true, // this module is isolated
  state,
  mutations,
  actions,
  getters,
};

export const strict = false;
