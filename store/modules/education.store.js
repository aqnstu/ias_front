import { API, sendGetRequest, sendPostRequest } from "../../interfaces/request";
import { EDU_TYPE, ORGANIZATION_TYPE } from "../../interfaces/entities";
import axios from "axios";

const state = () => ({});

export const getters = {};

export const mutations = {};

export const actions = {
  async getList({ commit }, { entityType, id, year, page, pageSize }) {
    let result = null;
    switch (entityType) {
      case EDU_TYPE.TARGET_STUDY:
        result = await sendGetRequest(API.HOST + '/api/get/target_study_request');
        break;
      case EDU_TYPE.GRADUATE_EDU:
        result = await sendGetRequest(API.HOST + `/api/list/graduate?year=${year}&page=${page}&page_size=${pageSize}`);
        break;
      case EDU_TYPE.GRADUATE_EDU_PIVOT:
        result = await sendGetRequest(API.HOST + '/api/pivot/graduate');
        break;
      case EDU_TYPE.GRADUATE_EDU_LINES:
        result = await sendGetRequest(API.HOST + '/api/get/lines/graduate_comps/' + id);
        break;
    }

    return result ? result.data : [];
  },
  async saveEntity({ commit }, { form, entityType, requestId, is_closed, year}) {
    switch (entityType) {
      case EDU_TYPE.TARGET_STUDY:
        return await sendPostRequest(API.HOST + `/api/set/target_study_request/${requestId}`,
          EDU_TYPE.convertRequestToOutput(form));
      case EDU_TYPE.GRADUATE_EDU:
        return await sendPostRequest(API.HOST + `/api/set/graduate/${requestId}`,
          EDU_TYPE.convertEduGraduateToOutput(form));
      case EDU_TYPE.GRADUATE_EDU_LINES:
        return await sendPostRequest(API.HOST + `/api/set/lines/graduate_comps/${requestId}`,
          ORGANIZATION_TYPE.convertObjectToArray(form.tableData));
      case EDU_TYPE.GRADUATE_POLL:
        return await sendPostRequest(API.HOST + `/api/set_status/graduate`, `year=${year}&is_closed=${is_closed}`);
      case EDU_TYPE.WORK_PROF:
        return await sendPostRequest(API.HOST + `/api/set/work_prof/${requestId}`, form );
    }
    return null;
  },
  async copyEntity({ commit }, { entityType, entityId }) {
    switch (entityType) {
      case EDU_TYPE.GRADUATE_EDU:
        return await sendPostRequest(API.HOST + `/api/copy/graduate/${entityId}`);
    }
    return null;
  },
  async delEntity({ commit }, { entityType, entityId }) {
    switch (entityType) {
      case EDU_TYPE.GRADUATE_EDU:
        return await sendPostRequest(API.HOST + `/api/del/graduate/${entityId}`);
    }
    return null;
  },
};

export default {
  namespaced: true, // this module is isolated
  state,
  mutations,
  actions,
  getters,
};

export const strict = false;
