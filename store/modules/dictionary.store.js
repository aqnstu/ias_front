import { USER_ROLE } from "../../interfaces/entities";
import { API, sendGetRequest } from "../../interfaces/request";

const state = () => ({
  lists: {
    regions: [],
    districts: [],
    organizations: [],
    investProject: [],
    opf: [],
    eduType: [],
    eduTypeWS: [],
    specialty: [], // ОКСО
    specialtyVO: [],
    specialtySPO: [],
    workProf: [],
    employerActivity: [],
    worldSkills: [], // World-Skills
    economicCode: [], // ОКВЭД
    occupationClassifier: [], // ОКЗ
    professionClassifier: [], // ОКПДТР
    organisationOwnershipForm: [], // ОКФ
    university: [],
    spoOrg: [],
    requestStatus: [],
    requestKCPStatus: [],
    vpoSpoStatus: [],
    typeSpo: [],
    specialtyKCP: [],
    organizationsKCP: [],
    months: [],
    competences: [],
    okved_partitions: [], //Разделы ОКВЭД
    type_ser_line_by_okved: [], //Соотрветствие прогноза СЭР
    loaded_data_sets: [],
  },
});

export const getters = {
  organization(state) {
    return organisationId => state.lists.organizations.filter(item => {
      return +item.id === +organisationId
    }) || [];
  }
};

export const mutations = {
  setDictionary(state, data = { dictionary, value }) {
    //console.log(state, data);
    state.lists[data.dictionary] = data.value;
  },
  reset(state) {
    state.lists = {
      regions: [],
      districts: [],
      organizations: [],
      investProject: [],
      specialty: [],
      specialtyVO: [],
      specialtySPO: [],
      workProf: [],
      employerActivity: [],
      opf: [],
      eduType: [],
      eduTypeWS: [],
      worldSkills: [],
      economicCode: [],
      occupationClassifier: [],
      professionClassifier: [],
      organisationOwnershipForm: [],
      university: [],
      spoOrg: [],
      requestStatus: [],
      requestKCPStatus: [],
      vpoSpoStatus: [],
      typeSpo: [],
      specialtyKCP: [],
      organizationsKCP: [],
      months: [],
      competences: [],
      okved_partitions: [],
      type_ser_line_by_okved: [],
      loaded_data_sets : [],
    };
  },
};

export const actions = {
  async initDictionaries({ commit, dispatch }, role) {
    switch (role) {
      case USER_ROLE.EMPLOYER:
      case USER_ROLE.EDU_ORGANIZATION:
      case USER_ROLE.MINISTRY_LABOR_SOCIAL_POLICY:
      default: {
        await dispatch('initDistrictList');
        await dispatch('initRegionList');
        await dispatch('initOrganizationsList');
        await dispatch('initInvestProjectList');
        await dispatch('initOpfList');
        await dispatch('initOrganisationOwnershipFormList');
        await dispatch('initSpecialtyList');
        await dispatch('initSpecialtyVOList');
        await dispatch('initSpecialtySPOList');
        await dispatch('initWorkProfList');
        await dispatch('initEmployerActivityList');
        await dispatch('initEduTypeList');
        await dispatch('initEduTypeWSList');
        await dispatch('initWorldSkillsList');
        await dispatch('initEconomicCodeList');
        await dispatch('initOccupationClassifierList');
        await dispatch('initProfessionClassifierList');
        await dispatch('initUniversityList');
        await dispatch('initSpoOrgList');
        await dispatch('initRequestStatusList');
        await dispatch('initRequestKCPStatusList');
        await dispatch('initVpoSpoStatusList');
        await dispatch('initTypeSpoList');
        await dispatch('initSpecialtyKCPList');
        await dispatch('initOrganizationsKCPList');
        await dispatch('initMonthList');
        await dispatch('initCompetencesSPOList');
        await dispatch('initTypeSerLineByOkvedList');
        await dispatch('initOkvedPartitionList');
        await dispatch('initLoadedDataSets');
      }
        break;
    }
  },
  async initRegionList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/region');
    commit('setDictionary', {
      dictionary: 'regions',
      value: response.data
    });
  },
  async initDistrictList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/mrigo');
    commit('setDictionary', {
      dictionary: 'districts',
      value: response.data
    });
  },
  async initInvestProjectList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/invest_proj');
    commit('setDictionary', {
      dictionary: 'investProject',
      value: response.data
    });
  },
  async initOpfList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/okopf');
    commit('setDictionary', {
      dictionary: 'opf',
      value: response.data
    });
  },
  async initSpecialtyList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/okso');
    commit('setDictionary', {
      dictionary: 'specialty',
      value: response.data
    });
  },
  async initSpecialtyVOList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/vw_okso_vo');
    commit('setDictionary', {
      dictionary: 'specialtyVO',
      value: response.data
    });
  },
  async initSpecialtySPOList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/okso_work');
    commit('setDictionary', {
      dictionary: 'specialtySPO',
      value: response.data
    });
  },
  async initWorkProfList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/work_prof');
    commit('setDictionary', {
      dictionary: 'workProf',
      value: response.data
    });
  },
  async initEmployerActivityList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/type_org_activity');
    commit('setDictionary', {
      dictionary: 'employerActivity',
      value: response.data
    });
  },
  async initEduTypeList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/educ_program_type');
    commit('setDictionary', {
      dictionary: 'eduType',
      value: response.data
    });
  },
  async initEduTypeWSList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/educ_program_type_ws');
    commit('setDictionary', {
      dictionary: 'eduTypeWS',
      value: response.data
    });
  },
  async initWorldSkillsList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/world_skills');
    commit('setDictionary', {
      dictionary: 'worldSkills',
      value: response.data
    });
  },
  async initEconomicCodeList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/okved');
    commit('setDictionary', {
      dictionary: 'economicCode',
      value: response.data
    });
  },
  async initOccupationClassifierList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/okz');
    commit('setDictionary', {
      dictionary: 'occupationClassifier',
      value: response.data
    });
  },
  async initOrganisationOwnershipFormList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/okf');
    commit('setDictionary', {
      dictionary: 'organisationOwnershipForm',
      value: response.data
    });
  },
  async initProfessionClassifierList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/okpdtr');
    commit('setDictionary', {
      dictionary: 'professionClassifier',
      value: response.data
    });
  },
  async initOrganizationsList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/organizations');
    commit('setDictionary', {
      dictionary: 'organizations',
      value: response.data
    });
  },
  async initUniversityList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/vw_organization_vo');
    commit('setDictionary', {
      dictionary: 'university',
      value: response.data
    });
  },
  async initSpoOrgList({ commit }) {
    //if (this.$store.state.user.userData.id_organization) {
      const response = await sendGetRequest(API.HOST + '/api/refs/vw_organization_spo');
      commit('setDictionary', {
        dictionary: 'spoOrg',
        value: response.data
      });
    // }
    // else{
    //   const response = await sendGetRequest(API.HOST + `/api/ref_one/vw_organization_spo/${this.$store.state.user.userData.id_organization}`);
    //   commit('setDictionary', {
    //     dictionary: 'spoOrg',
    //     value: response.data
    //   });
    // }
  },
  async initRequestStatusList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/target_study_request_status');
    commit('setDictionary', {
      dictionary: 'requestStatus',
      value: response.data
    });
  },
  async initRequestKCPStatusList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/kcp_request_status');
    commit('setDictionary', {
      dictionary: 'requestKCPStatus',
      value: response.data
    });
  },
  async initVpoSpoStatusList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/type_vpospo_status');
    commit('setDictionary', {
      dictionary: 'vpoSpoStatus',
      value: response.data
    });
  },
  async initTypeSpoList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/type_spo');
    commit('setDictionary', {
      dictionary: 'typeSpo',
      value: response.data
    });
  },
  async initSpecialtyKCPList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/okso_kcp');
    commit('setDictionary', {
      dictionary: 'specialtyKCP',
      value: response.data
    });
  },
  async initOrganizationsKCPList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/okso_kcp_org');
    commit('setDictionary', {
      dictionary: 'organizationsKCP',
      value: response.data
    });
  },
  async initMonthList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/month');
    commit('setDictionary', {
      dictionary: 'months',
      value: response.data
    });
  },
  async initCompetencesSPOList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/spo_comp');
    commit('setDictionary', {
      dictionary: 'competences',
      value: response.data
    });
  },
  async initOkvedPartitionList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/okved_partitions');
    commit('setDictionary', {
      dictionary: 'okved_partitions',
      value: response.data
    });
  },
  async initTypeSerLineByOkvedList({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/type_ser_line_by_okved');
    commit('setDictionary', {
      dictionary: 'type_ser_line_by_okved',
      value: response.data
    });
  },
  async initLoadedDataSets({ commit }) {
    const response = await sendGetRequest(API.HOST + '/api/list/loaded');
    commit('setDictionary', {
      dictionary: 'loaded_data_sets',
      value: response.data
    });
  },
  async getCitiesByRegion({ commit }, { region }) {
    const response = await sendGetRequest(API.HOST + '/api/refs/punkt/' + region);
    return response ? response.data : [];
  },
};

export default {
  namespaced: true, // this module is isolated
  state,
  getters,
  mutations,
  actions,
};

export const strict = false;
