import {API, sendGetRequest, sendPostFileRequest, sendPostRequest} from "../../interfaces/request";
import {INVESTING_TYPE, LOT} from "../../interfaces/entities";
import axios from "axios";

const state = () => ({
  lists: {
    projects: [],
  },
});

export const getters = {};

export const mutations = {
  setList(state, data = { list, value }) {
    state.lists[data.list] = data.value;
  },
  reset(state) {
    state.lists = {
      projects: [],
    };
  },
};

export const actions = {
  async getList({ commit }, { entityType }) {
    const result = await sendGetRequest(API.HOST + '/api/list/invest_proj', {});
    commit('setList', {
      list: 'projects',
      value: result.data
    });
  },
  async saveFile({ commit }, {entityType, file}){
    switch (entityType) {
      case INVESTING_TYPE.PROJECT:
        return await sendPostFileRequest(API.HOST + `/api/upload/inv_proj`, file);
    }

    return null;
  },
  async getLines({ commit }, { entityType, projectId }) {
    let result = null;
    switch (entityType) {
      case INVESTING_TYPE.PROJECT_LINE_YEARS:
        result = await sendGetRequest(API.HOST + '/api/get/lines/invest_proj_years/' + projectId);
        break;
      case INVESTING_TYPE.PROJECT_LINE_INDICATORS:
        result = await sendGetRequest(API.HOST + '/api/get/lines/invest_proj_line/' + projectId);
        break;
      case INVESTING_TYPE.PROJECT_LINE_PEOPLES:
        result = await sendGetRequest(API.HOST + '/api/get/lines/invest_proj_line_add/' + projectId);
        break;
      case INVESTING_TYPE.PROJECT_LINE_OKVED:
        result = await sendGetRequest(API.HOST + '/api/get/lines/invest_proj_okved/' + projectId);
        break;
    }
    return result ? result.data : [];
  },
  async saveEntity({ commit }, { form, deleted, entityType, projectId }) {
    switch (entityType) {
      case INVESTING_TYPE.PROJECT:
        return await sendPostRequest(API.HOST + '/api/set/invest_proj/' + projectId,
          INVESTING_TYPE.convertProjectToOutput(form));
      case INVESTING_TYPE.PROJECT_LINE_INDICATORS:
        return await sendPostRequest(API.HOST + '/api/set/lines/invest_proj_line/' + projectId,
          INVESTING_TYPE.convertProjectLineIndicatorToOutput(form));
      case INVESTING_TYPE.PROJECT_LINE_PEOPLES:
        //по каждой строке
        console.log(form);
        console.log(INVESTING_TYPE.convertProjectLinePeoplesToOutput2(form));
        console.log(deleted);
        let a = await sendPostRequest(API.HOST + `/api/set2/lines/invest_proj_line_add/${projectId}/0`,
          INVESTING_TYPE.convertProjectLinePeoplesToOutput2(form));
        if (deleted){
          console.log('deleted!');
          return await sendPostRequest(API.HOST + `/api/set2/lines/invest_proj_line_add/${projectId}/1`,
            INVESTING_TYPE.convertProjectLinePeoplesToOutput(deleted));
        }
        else {
          return a;
        }
        // return await sendPostRequest(API.HOST + '/api/del2/lines/invest_proj_line_add/' + projectId,
        //   INVESTING_TYPE.convertProjectLinePeoplesToOutput(deleted));
      case INVESTING_TYPE.PROJECT_LINE_OKVED:
        return await sendPostRequest(API.HOST + '/api/set/lines/invest_proj_okved/' + projectId,
          INVESTING_TYPE.convertOkvedToOutput(form));
    }
    return null;
  },
  async delEntity({ commit }, { projectId }) {
    return await sendPostRequest(API.HOST + '/api/del/invest_proj', `id=${projectId}`);
  },
  async getDownloadInvProj({ commit }) {
    return await sendGetRequest(API.HOST + `/api/gen_inv_proj`);
  },
};

export default {
  namespaced: true, // this module is isolated
  state,
  mutations,
  actions,
  getters,
};

export const strict = false;
