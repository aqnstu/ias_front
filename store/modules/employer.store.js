import {
  API,
  sendPostRequest,
  sendGetRequest,
  convertResponseToInner,
  sendPostFileRequest
} from "../../interfaces/request";
import {INVESTING_TYPE, ORGANIZATION_TYPE} from "../../interfaces/entities";
import axios from "axios";

const state = () => ({
  lists: {
    employers: [],
    customers: [],
    vpo: [],
    spo: [],
  },
});

export const getters = {};

export const mutations = {
  setList(state, data = { list, value }) {
    state.lists[data.list] = data.value;
  },
  reset(state) {
    state.lists = {
      employers: [],
      customers: [],
      vpo: [],
      spo: [],
    };
  },
};

export const actions = {
  async getList({ commit }, { entityType, year, id_mrigo, page_num}) {
    let result = null;
    let listName = null;
    switch (entityType) {
      case ORGANIZATION_TYPE.EMPLOYER:
        listName = 'employers';
        result = await sendGetRequest(API.HOST + '/api/list/organization_poll?year=' + year + '&id_mrigo=' + id_mrigo +'&page_num='+page_num,
          { year: year, id_mrigo: id_mrigo, page_num: page_num });
        return convertResponseToInner(result.data);
        break;
      case ORGANIZATION_TYPE.CNTS:
        listName = 'cnts';
        result = await sendGetRequest(API.HOST + '/api/list/organization_poll_cnts?year=' + year + '&id_mrigo=' + id_mrigo,
          { year: year, id_mrigo: id_mrigo });
        console.log(result.data)
        return result.data;
        break;
      case ORGANIZATION_TYPE.VPO_POLLS:
        listName = 'vpo';
        result = await sendGetRequest(API.HOST + '/api/list/vpo_poll', {});
        break;
      case ORGANIZATION_TYPE.SPO_POLLS:
        listName = 'spo';
        result = await sendGetRequest(API.HOST + '/api/list/spo_poll', {});
        break;
    }
    commit('setList', {
      list: listName,
      value: result ? result.data : []
    });
  },
  async getLines({ commit }, { entityType, formId }) {
    let result = null;
    switch (entityType) {
      case ORGANIZATION_TYPE.EMPLOYER_LINE_YEARS:
        result = await sendGetRequest(API.HOST + '/api/get/lines/organization_form_years/' + formId);
        break;
      case ORGANIZATION_TYPE.EMPLOYER_LINE_PEOPLES:
        result = await sendGetRequest(API.HOST + '/api/get/lines/organization_form_req_lines/' + formId);
        break;
      case ORGANIZATION_TYPE.EMPLOYER_LINE_PEOPLES_INVEST:
        result = await sendGetRequest(API.HOST + '/api/get/lines/organization_form_invest_req_lines/' + formId);
        break;
      case ORGANIZATION_TYPE.EMPLOYER_LINE_LINE_OKVED:
        result = await sendGetRequest(API.HOST + '/api/get/lines/organization_okved/' + formId);
        break;
      case ORGANIZATION_TYPE.EMPLOYER_LINE_PROFESSION_PEOPLES:
        result = await sendGetRequest(API.HOST + '/api/get/lines/organization_form_pks_lines/' + formId);
        break;
      case ORGANIZATION_TYPE.EMPLOYER_LINE_ORGANIZATION_AGE:
        result = await sendGetRequest(API.HOST + '/api/get/lines/organization_form_age_lines/' + formId);
        break;
      case ORGANIZATION_TYPE.EMPLOYER_LINE_ORGANIZATION_SEX:
        result = await sendGetRequest(API.HOST + '/api/get/lines/organization_form_sex_lines/' + formId);
        break;
      case ORGANIZATION_TYPE.EMPLOYER_LINE_ORGANIZATION_EDU:
        result = await sendGetRequest(API.HOST + '/api/get/lines/organization_form_educ_lines/' + formId);
        break;
      case ORGANIZATION_TYPE.EMPLOYER_LINE_ORGANIZATION_EDU_WORLDSKILLS:
        result = await sendGetRequest(API.HOST + '/api/get/lines/organization_form_ws_lines/' + formId);
        break;
      // ВПО
      case ORGANIZATION_TYPE.VPO_SPO_LINE:
        result = await sendGetRequest(API.HOST + '/api/get/lines/vpospo_line/' + formId);
        break;
    }
    return result ? result.data : [];
  },
  async saveEntity({ commit }, { form, entityType, formId }) {
    console.log('saveEntity', form, entityType, formId);
    switch (entityType) {
      // Organization
      case ORGANIZATION_TYPE.CNTS:
        return await sendPostRequest(API.HOST + '/api/save/organization_poll_cnts' ,
          ORGANIZATION_TYPE.convertCntsToOutput(form));
      case ORGANIZATION_TYPE.ORGANIZATION:
        return await sendPostRequest(API.HOST + '/api/set2/organization_form/' + formId,
          ORGANIZATION_TYPE.convertOrganizationToOutput(form));
      case ORGANIZATION_TYPE.ORGANIZATION_NAME:
        if (formId) {
          return await sendPostRequest(API.HOST + '/api/set/organization/' + formId,
            ORGANIZATION_TYPE.convertOrganizationNameToOutput(form));
        }
        else {
          return await sendPostRequest(API.HOST + '/api/set2/organization_for_invest',
            ORGANIZATION_TYPE.convertOrganizationNameToOutput(form));
        }
      case ORGANIZATION_TYPE.EMPLOYER_LINE_LINE_OKVED:
        return await sendPostRequest(API.HOST + '/api/set/lines/organization_okved/' + formId,
          ORGANIZATION_TYPE.convertOkvedToOutput(form));
      case ORGANIZATION_TYPE.EMPLOYER_LINE_PEOPLES:
        return await sendPostRequest(API.HOST + '/api/set/lines/organization_form_req_lines/' + formId,
          ORGANIZATION_TYPE.convertObjectToArray(form.peopleLines));
      //
      case ORGANIZATION_TYPE.EMPLOYER_LINE_PEOPLES_INVEST:
        return await sendPostRequest(API.HOST + '/api/set/lines/organization_form_invest_req_lines/' + formId,
          ORGANIZATION_TYPE.convertObjectToArray(form.peopleLinesInvest));
      //
      case ORGANIZATION_TYPE.EMPLOYER_LINE_PROFESSION_PEOPLES:
        return await sendPostRequest(API.HOST + '/api/set/lines2/organization_form_pks_lines/' + formId,
          ORGANIZATION_TYPE.convertObjectToArray(form.peopleProfessionLines));
      case ORGANIZATION_TYPE.EMPLOYER_LINE_ORGANIZATION_AGE:
        return await sendPostRequest(API.HOST + '/api/set/lines2/organization_form_age_lines/' + formId,
          ORGANIZATION_TYPE.convertObjectToArray(form.organisationWorkersByAge));
      case ORGANIZATION_TYPE.EMPLOYER_LINE_ORGANIZATION_SEX:
        return await sendPostRequest(API.HOST + '/api/set/lines2/organization_form_sex_lines/' + formId,
          ORGANIZATION_TYPE.convertObjectToArray(form.organisationWorkersBySex));
      // EDU
      case ORGANIZATION_TYPE.EMPLOYER_LINE_ORGANIZATION_EDU:
        return await sendPostRequest(API.HOST + '/api/set/lines/organization_form_educ_lines/' + formId,
          ORGANIZATION_TYPE.convertObjectToArray(form.organisationEducationLines));
      case ORGANIZATION_TYPE.EMPLOYER_LINE_ORGANIZATION_EDU_WORLDSKILLS:
        return await sendPostRequest(API.HOST + '/api/set/lines/organization_form_ws_lines/' + formId,
          ORGANIZATION_TYPE.convertObjectToArray(form.organisationWorldskillsLines));
      case ORGANIZATION_TYPE.VPO_SPO_STATUS:
        return await sendPostRequest(API.HOST + '/api/set/vpospo/' + formId,
          { id_type_vpospo_status: form.organisationTypeStatus });
      case ORGANIZATION_TYPE.VPO_SPO_LINE:
        return await sendPostRequest(API.HOST + '/api/set/lines/vpospo_line/' + formId,
          ORGANIZATION_TYPE.convertObjectToArray(form.tableData));
      case ORGANIZATION_TYPE.VO_SPO:
        return await sendPostRequest(API.HOST + '/api/set/organization/' + formId,
          ORGANIZATION_TYPE.convertVoOrSpoToOutput(form));
    }
    return null;
  },
  async saveFile({ commit }, {entityType, file, year, mrigo}) {
    switch (entityType) {
      case ORGANIZATION_TYPE.POLLS:
        if (mrigo)
          return await sendPostFileRequest(API.HOST + `/api/upload/organization_polls?year_for=${year}`, file);
        else
          return await sendPostFileRequest(API.HOST + `/api/upload/organization_polls?year_for=${year}&id_mrigo=${mrigo}`, file);
    }
    return null;
  },

};

export default {
  namespaced: true, // this module is isolated
  state,
  mutations,
  actions,
  getters,
};

export const strict = false;
