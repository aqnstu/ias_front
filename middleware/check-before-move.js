export default function ({ store }) {

  store.app.router.beforeEach((to, from, next) => {
    const isLoggedIn = store.getters.isLoggedIn;

    if (isLoggedIn && to.name === 'login') {
      next('/cabinet');
    }

    if (isLoggedIn || to.name === 'login' || to.name === 'index') {
      next();
    } else {
      next('/login');
    }
  })

}
