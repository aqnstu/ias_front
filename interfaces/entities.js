export const USER_ROLE = {
  EMPLOYER: 'EMPLOYER', // Юридическое лицо (работодатель)
  MUNICIPAL_AUTHORITY: 'MUNICIPAL_AUTHORITY', // Муниципальный орган власти
  EDU_ORGANIZATION: 'EDU_ORGANIZATION', // Образовательная организация
  MINISTRY_LABOR_SOCIAL_POLICY: 'MINISTRY_LABOR_SOCIAL_POLICY', // Министерство труда и социальной политики
  MINISTRY_EDU: 'MINISTRY_EDU', // Министерство образования
  ADMIN: 'ADMIN', // Администратор
};

export const EDU_TYPE = {
  HIGHER: 'HIGHER', // Высшее образование
  SECONDARY: 'SECONDARY', // Среднее образование
  REQUEST_TYPE_EMPLOYER: 1,
  REQUEST_TYPE_CUSTOMER: 2,
  //
  TARGET_STUDY: 'TARGET_STUDY',
  GRADUATE_EDU: 'GRADUATE_EDU',
  GRADUATE_EDU_PIVOT: 'GRADUATE_EDU_PIVOT',
  GRADUATE_EDU_LINES: 'GRADUATE_EDU_LINES',
  GRADUATE_POLL: 'GRADUATE_POLL',
  WORK_PROF: 'WORK_PROF',
  // methods:
  convertRequestToOutput(formInner) {
    return {
      id_organization: formInner.organisationID,
      id_okopf_employer: formInner.organisationForm,
      id_region: formInner.organisationRegion,
      id_mrigo: formInner.organisationDistrict,
      id_type_activity_employer: formInner.activityType,
      id_okso: formInner.specialty,
      id_educ_organization: formInner.university,
      work_duration: formInner.workDuration,
      job_name: formInner.post,
      job_descr: formInner.jobFunctions,
      salary_max: formInner.jobSalaryMax,
      salary_min: formInner.jobSalaryMin,
      salary_comment: formInner.jobSalaryComment,
      additional_info: formInner.employerAdvInfo,
      email: formInner.contactsEmail,
      phone: formInner.contactsPhone,
      id_target_study_request_status: formInner.requestStatus,
      support_info: formInner.supportInfo,
      practice_info: formInner.practiceInfo,
      job_garanty: formInner.jobGaranty,
      id_type_target_request: formInner.requestType,
      id_okopf_customer: formInner.organisationFormCustomer,
      id_type_activity_customer: formInner.activityTypeCustomer,
    };
  },
  convertEduGraduateToOutput(formInner) {
    return {
      cnt: formInner.graduateCount,
      id_okso: formInner.isOvz?null:formInner.specialty,
      id_organization: formInner.organisationId,
      id_mrigo: formInner.livingPlace,
      id_punct: formInner.livingCity,
      month: formInner.graduateMonth,
      year: formInner.year,
      is_ovz: formInner.isOvz?1:0,
      add_inf: formInner.addInf,
      id_work_profs: formInner.isOvz?formInner.workProf:null,
      id_region: formInner.idRegion,
      country_remark: formInner.countryRemark,
    }
  },
};

export const ORGANIZATION_TYPE = {
  EMPLOYER: 'EMPLOYER',
  CUSTOMER: 'CUSTOMER',
  CNTS: 'CNTS',
  PROJECT: 'PROJECT',
  ORGANIZATION: 'ORGANIZATION',
  ORGANIZATION_NAME: 'ORGANIZATION_NAME',
  EMPLOYER_LINE_YEARS: 'EMPLOYER_LINE_YEARS',
  EMPLOYER_LINE_PEOPLES: 'EMPLOYER_LINE_PEOPLES',
  EMPLOYER_LINE_PEOPLES_INVEST: 'EMPLOYER_LINE_PEOPLES_INVEST',
  //
  EMPLOYER_LINE_LINE_OKVED: 'EMPLOYER_LINE_LINE_OKVED',
  EMPLOYER_LINE_PROFESSION_PEOPLES: 'EMPLOYER_LINE_PROFESSION_PEOPLES',
  EMPLOYER_LINE_ORGANIZATION_AGE: 'EMPLOYER_LINE_ORGANIZATION_AGE',
  EMPLOYER_LINE_ORGANIZATION_SEX: 'EMPLOYER_LINE_ORGANIZATION_SEX',
  //
  EMPLOYER_LINE_ORGANIZATION_EDU: 'EMPLOYER_LINE_ORGANIZATION_EDU',
  EMPLOYER_LINE_ORGANIZATION_EDU_WORLDSKILLS: 'EMPLOYER_LINE_ORGANIZATION_EDU_WORLDSKILLS',
  POLLS: 'POLLS',
  // ВПО
  VPO_POLLS: 'VPO_POLLS',
  VPO_SPO_STATUS: 'VPO_SPO_STATUS',
  VPO_SPO_LINE: 'VPO_SPO_LINE',
  SPO_POLLS: 'SPO_POLLS',
  //
  VO_SPO: 'VO_SPO',

  // methods:
  convertCntsToOutput(formInner) {
    return {
      year: formInner.year,
      id_mrigo: formInner.id_mrigo,
      cnt_in_poll: formInner.cnt_in_poll,
      cnt_not_req: formInner.cnt_not_req,
    };
  },
  convertOrganizationToOutput(formInner) {
    return {
      salary: formInner.organisationMonthSalary,
      cnt: formInner.organisationPeoples,
      id_okfs: formInner.organisationOwnershipForm,
    };
  },
  convertOrganizationNameToOutput(formInner) {
    return {
      name: formInner.organization_name,
      inn: formInner.organization_inn,
      ogrn: formInner.organization_ogrn,
    };
  },
  convertVoOrSpoToOutput(formInner) {
    return {
      name: formInner.organisationName,
      id_mrigo: formInner.organisationDistrict,
      inn: formInner.organisationINN,
      ogrn: formInner.organisationOGRN,
      adress: formInner.organisationAddress,
      head: formInner.organisationHead,
      email: formInner.organisationEmail,
      phone: formInner.organisationPhone,
      url: formInner.organisationSite,
      is_educ: formInner.is_educ,
      is_spo: formInner.is_spo,
      id_okfs: formInner.id_okfs,
    };
  },
  convertObjectToArray(object) {
    return [
      ...object
    ];
  },
  convertOkvedToOutput(formInner) {
    let result = [];
    for (let [key, value] of Object.entries(formInner.organisationEconomicCode)) {
      result.push({ id_organization: formInner.organisationID, id_okved: value });
    }
    return result;
  },
};

export const INVESTING_TYPE = {
  PROJECT: 'PROJECT',
  PROJECT_LINE_YEARS: 'PROJECT_LINE_YEARS',
  PROJECT_LINE_INDICATORS: 'PROJECT_LINE_INDICATORS',
  PROJECT_LINE_PEOPLES: 'PROJECT_LINE_PEOPLES',
  PROJECT_LINE_OKVED: 'PROJECT_LINE_OKVED',
  // methods:
  convertProjectToOutput(formInner) {
    return {
      name: formInner.projectName,
      id_organization: formInner.projectInvestorID,
      invest_sum: formInner.projectFinancing,
      date_start: formInner.projectRealizationFrom,
      date_finish: formInner.projectRealizationTo,
    };
  },
  convertProjectLineIndicatorToOutput(formInner) {
    return [
      ...formInner
    ];
  },
  convertProjectLinePeoplesToOutput(formInner) {
    return [
      ...formInner
    ];
  },
  convertProjectLinePeoplesToOutput2(formInner) {
    if (!formInner)
        return [];
    return formInner.filter((x)=>(x.modified == 1)).
            map((x)=>(Object.keys(x).
                filter(
                (x0)=>(x0.substr(0,2) == 'y_'))
              ).map((x1)=>({year_for:+x.year_start+(+x1.substr(2,2)),
                value: +x[x1],
                id_okpdtr: x.id_okpdtr,
                id_okz: x.id_okz,
                id_okved: x.id_okved,
                id_okpdtr_src: x.id_okpdtr_src,
                id_type: x.id_type,
                id_invest_proj: x.id_invest_proj,
              }))
          ).flat();
  },
  convertOkvedToOutput(formInner) {
    let result = [];
    for (let [key, value] of Object.entries(formInner.organisationEconomicCode)) {
      result.push({ id_invest_proj: formInner.projectID, id_okved: value });
    }
    return result;
  },
};

export const LOT = {
  SCENARIO_LIST: 'SCENARIO_LIST',
  SCENARIO_TAB: 'SCENARIO_TAB',
  RESULT_LINE: 'RESULT_LINE',
  RESULT_YEARS: 'RESULT_YEARS',
  FORMAT_WORD: 'FORMAT_WORD',
  FORMAT_EXCEL: 'FORMAT_EXCEL',
  KCP_LOT_PROCESS: 'KCP_LOT_PROCESS',
  UGSN_RECALC: 'UGSN_RECALC',
  KCP_OKSO_RESULT: 'KCP_OKSO_RESULT',
  KCP_LOT_RESULT: 'KCP_LOT_RESULT',
  KCP_LOT_RESULT2: 'KCP_LOT_RESULT2',
  KCP_LOT_REQUEST: 'KCP_LOT_REQUEST',
  KCP_LOT_REQUEST2: 'KCP_LOT_REQUEST2',
  KCP_LOT_REQUEST_ONE: 'KCP_LOT_REQUEST_ONE',
  KCP_LOT_REQUEST_PARAMS: 'KCP_LOT_REQUEST_PARAMS',
  KCP_LOT_REQUEST_SRC: 'KCP_LOT_REQUEST_SRC',
  KCP_LOT_REQUEST_PARAMS_OKSO: 'KCP_LOT_REQUEST_PARAMS_OKSO',
  KCP_LOT_LIST: 'KCP_LOT_LIST',
  ADAPTER_CLASS: 'ADAPTER_CLASS',
  ADAPTER_CLASS_LIST: 'ADAPTER_CLASS_LIST',
  ADAPTER_PIVOT: 'ADAPTER_PIVOT',
  ADAPTER_PIVOT_BY_SCENARIO: 'ADAPTER_PIVOT_BY_SCENARIO',
  POLL_VOSPO_PIVOT: 'POLL_VOSPO_PIVOT',
  POLL_SCHOOL_PIVOT: 'POLL_SCHOOL_PIVOT',
  PIVOT_PROGNOZ_KCP: 'PIVOT_PROGNOZ_KCP',
  PROCESS_UGSN: 'PROCESS_UGSN',
  PIVOT_PROGNOZ_UGSN: 'PIVOT_PROGNOZ_UGSN', //прогноз потребности по УГСН
  PIVOT_PROGNOZ_WP: 'PIVOT_PROGNOZ_WP', //прогноз потребности по Рабочим профессиям
  PIVOT_PROGNOZ_OKPDTR: 'PIVOT_PROGNOZ_OKPDTR',
  UGS_DEDUCT: 'UGS_DEDUCT',
  EXPERT_VALS: 'EXPERT_VALS',
  EXPERT_VALS_OKVED: 'EXPERT_VALS_OKVED',
  TYPE_SER_OKVED: 'TYPE_SER_OKVED',
  SER_LINES: 'SER_LINES',
  STAT_DATASETS: 'STAT_DATASETS',
  PIVOT_STAT_DATASET: 'PIVOT_STAT_DATASET',
  FORECAST_BTR: 'FORECAST_BTR',
  LOADED_BTR: 'LOADED_BTR',
  MONITORING_FORM12: 'MONITORING_FORM12',
  MONITORING_FORM12_PVT: 'MONITORING_FORM12_PVT',
  MONITORING_FORM13: 'MONITORING_FORM13',
  MONITORING_FORM14: 'MONITORING_FORM14',
  MONITORING_FORM15: 'MONITORING_FORM15',
  MONITORING_FORM16: 'MONITORING_FORM16',
  MONITORING_FORM17: 'MONITORING_FORM17',
  PIVOT_VACS: 'PIVOT_VACS',
  PIVOT_RESUME: 'PIVOT_RESUME',

  convertLotToOutput(formInner) {
    console.log(formInner);
    return {
      lot_number: formInner.lotNumber,
      id_okso: formInner.specialty,
      year: formInner.year,
      year_for: formInner.yearFor,
      total_value: formInner.totalValue,
      ovz_value: parseInt(formInner.ovzValue),
    };
  },
  convertRequestToOutput(formInner) {
    return {
      id_kcp_request_status: formInner.requestStatus,
      id_organization: formInner.organizationId,
      request_num: formInner.requestNum,
      year_for: formInner.yearFor,
    }
  },
  convertRequestLinesToOutput(formInner) {
    return [
      ...formInner
    ];
  },
  convertRequestOKSOToOutput(formInner) {
    return [
      ...formInner
    ];
  }
};


export const ADMIN = {
  VO: 'VO',
  SPO: 'SPO',
  REF_LIST: 'REF_LIST',
  REF_ENTITY: 'REF_ENTITY',
};
