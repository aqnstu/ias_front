import axios from 'axios'

export const API = {
  HOST: `${process.env.baseUrl}`,
  // ... other urls
};

// Common function
export async function sendGetRequest(address, params = {}) {
  return await axios.get(address, params)
    .then(response => {
      return response;
    })
    .catch(response => {
      console.error('catch response GET error', response);
      return null;
    })
    .finally(() => {
    });
}

export async function sendPostRequest(address, params = {}) {
  return await axios.post(address, params)
    .then(response => {
      return response;
    })
    .catch(response => {
      console.error('catch response POST error', response);
      return null;
    })
    .finally(() => {
    });
}

export async function sendPostFileRequest(address, file) {
  let formData = new FormData();
  formData.append('file', file);
  return await axios.post(address, formData, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
    .then(response => {
      return response;
    })
    .catch(response => {
      console.error('catch response POST error', response);
      return null;
    })
    .finally(() => {
    });
}

export async function sendAuthRequest(login, password) {
  return await sendPostRequest(API.HOST + '/api/auth', { login: login, pass: password });
}

export async function sendAuthRequestBy(token) {
  return await sendPostRequest(API.HOST + '/api/auth_key', { token: token });
}

export function convertResponseToInner(response) {
  let returnObj = { data: [], page: 0, pagesTotal: -1 };
  if (response && response.data) {
    if (Array.isArray(response.data)) {
      returnObj.data = response.data;
    }
    if (response.metadata) {
      returnObj.page = response.metadata.page_num;
      returnObj.pagesTotal = response.metadata.pages;
    }
  }
  return returnObj;
}

