export default {
  props: {
    tableData: { type: Array, default: () => [] },
  },
  computed: {
    yearList() {
      let result = {};
      const years = this.$store.state.lot.lists.resultYears;
      if (years.length > 0) {
        for (let [key, value] of Object.entries(years[0])) {
          if (key !== 'id_fc_scenario') {
            result[key] = value;
          }
        }
      }
      return result;
    },
  },
}
