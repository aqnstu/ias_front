export default {
  computed: {
    // Dictionaries:
    specialtyList() {
      return this.$store.state.dictionary.lists.specialty || [];
    },
    specialtyVOList() {
      return this.$store.state.dictionary.lists.specialtyVO || [];
    },
    specialtySPOList() {
      return this.$store.state.dictionary.lists.specialtySPO || [];
    },
    workProfList() {
      return this.$store.state.dictionary.lists.workProf || [];
    },
    employerActivityList() {
      return this.$store.state.dictionary.lists.employerActivity || [];
    },
    economicCodeList() { // ОКВЭД
      return this.$store.state.dictionary.lists.economicCode || [];
    },
    opfList() {
      return this.$store.state.dictionary.lists.opf || [];
    },
    districtList() {
      return this.$store.state.dictionary.lists.districts || [];
    },
    regionList() {
      if (this.$store.state.dictionary.lists.regions[0].id == 1)
          this.$store.state.dictionary.lists.regions.shift();
      return this.$store.state.dictionary.lists.regions || [];
    },
    eduTypeList() {
      return this.$store.state.dictionary.lists.eduType || [];
    },
    eduTypeWSList() {
      return this.$store.state.dictionary.lists.eduTypeWS || [];
    },
    worldSkillsList() {
      return this.$store.state.dictionary.lists.worldSkills || [];
    },
    investProjectList() {
      return this.$store.state.dictionary.lists.investProject || [];
    },
    organizationList() {
      return this.$store.state.dictionary.lists.organizations || [];
    },
    occupationClassifierList() { // ОКЗ
      return this.$store.state.dictionary.lists.occupationClassifier || [];
    },
    organisationOwnershipFormList() { // ОКФ
      return this.$store.state.dictionary.lists.organisationOwnershipForm || [];
    },
    professionClassifierList() { // ОКПДТР
      return this.$store.state.dictionary.lists.professionClassifier || [];
    },
    universityList() {
      return this.$store.state.dictionary.lists.university || [];
    },

    spoOrgsList() {
      return this.$store.state.dictionary.lists.spoOrg || [];
    },

    requestStatusList() {
      return this.$store.state.dictionary.lists.requestStatus || [];
    },
    requestKCPStatusList() {
      return this.$store.state.dictionary.lists.requestKCPStatus || [];
    },
    vpoSpoStatusList() {
      return this.$store.state.dictionary.lists.vpoSpoStatus || [];
    },
    typeSpoList() {
      return this.$store.state.dictionary.lists.typeSpo || [];
    },
    specialtyKCPList() {
      return this.$store.state.dictionary.lists.specialtyKCP || [];
    },
    organizationsKCPList() {
      return this.$store.state.dictionary.lists.organizationsKCP || [];
    },
    competencesSPOList() {
      return this.$store.state.dictionary.lists.competences || [];
    },
    okvedPartitionsList() {
      return this.$store.state.dictionary.lists.okved_partitions || [];
    },
    typeSerLineByOkvedList() {
      return this.$store.state.dictionary.lists.type_ser_line_by_okved || [];
    },
    loaded_data_sets() {
      return this.$store.state.dictionary.lists.loaded_data_sets || [];
    },

    // Common:
    //TODO: переделать на сравнение с 'y_'
    yearList() {
      let result = {};
      let years = this.years || [];
      if (years.length > 0) {
        for (let [key, value] of Object.entries(years[0])) {
          if (key !== 'id_form' && key !== 'year_start') {
            result[key] = value;
          }
        }
      }
      return result;
    },
    //с первого элемента
    yearList1() {
      let result = {};
      let years = this.years || [];
      if (years.length > 0) {
        for (let [key, value] of Object.entries(years[0])) {
          if (key !== 'id_form' && key !== 'year_start' && key !== 'y_0') {
            result[key] = value;
          }
        }
      }
      return result;
    },

  },
  methods: {
    close() {
      this.$emit('close');
    },
    save() {
      this.$emit('save', this.form);
    },
  },
}
